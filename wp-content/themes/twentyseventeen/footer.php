?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="wrap">
				<?php
				get_template_part( 'template-parts/footer/footer', 'widgets' );

				if ( has_nav_menu( 'social' ) ) :
					?>
					<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentyseventeen' ); ?>">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'social',
									'menu_class'     => 'social-links-menu',
									'depth'          => 1,
									'link_before'    => '<span class="screen-reader-text">',
									'link_after'     => '</span>' . twentyseventeen_get_svg( array( 'icon' => 'chain' ) ),
								)
							);
						?>
					</nav><!-- .social-navigation -->
					<?php
				endif;

				get_template_part( 'template-parts/footer/site', 'info' );
				?>
			</div><!-- .wrap -->
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<script type="text/javascript" >
jQuery(document).ready(function($) {

	// Bắt click button load more
    // Kiểm tra đang ở page bao nhiêu ?
    // Lấy đúng gửi ajax lên cho wp
    // Dùng getpost lấy đúng page của dữ liệu
    // Tạo html trả về cho ajax
    // Ajax render dữ liệu lên màn hình
    var ajax_url = "<?php echo admin_url( 'admin-ajax.php') ?>";
    var paged = 1;

    $( "#load-more" ).on( "click", function() {
        paged = paged + 1;
        var ul  = $(this).parent().find('ul');
        var loadmore = $(this);

        var data = {
            'action': 'loadmore',
            'paged': paged,
            'number': loadmore.attr('rel-sobai'),
            'loaibai': loadmore.attr('rel-post_type')
        };
        var request = $.ajax({
            url: ajax_url,
            method: "POST",
            data: data,
            dataType: "json"
        });
        request.done(function( data ) {
            if(data.hide_load_more){
                loadmore.hide('show');
            }
            ul.append(data.html);
        });
        request.fail(function( jqXHR, textStatus ) {
        });
    });
    
    var ajax_url = "<?php echo admin_url( 'admin-ajax.php') ?>";
    var data = {
        'action': 'loadmore',
        'paged': 3
    };
    var request = $.ajax({
        url: ajax_url,
        method: "POST",
        data: data,
        dataType: "json"
    });
    request.done(function( data ) {
        
    });
    request.fail(function( jqXHR, textStatus ) {
    });

});



</script>

</html>
