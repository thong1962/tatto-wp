<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Demo ajax</title>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
     <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
</head>
<body>
    <div class="container-fluid">
    <h1>Demo Ajax</h1>
  <!-- hien thi ten len mh dung appent -->
     <h2 id="message">Xin Chào:</h2>
     <form id="form_login">
        <div class="row">
            <div class="col">
            <input id="first_name" name="first_name" type="text" class="form-control" placeholder="First name">
            </div>
            <div class="col">
            <input id="center_name" name="center_name" type="text" class="form-control" placeholder="Center name">
            </div>
             <div class="col">
            <input id="last_name" name="last_name" type="text" class="form-control" placeholder="Last name">
            </div>
             <div class="col">
            <input id="password" name="password" type="password" class="form-control" placeholder="password">
            </div>
        </div>
        <br>
        <a id="submit_form" class="btn btn-primary" href="javascript:void(0)">Submit</a>
    </form>
</div>
<script>
    //
    function showValues() {
    var fields = $( "#form_login" ).serializeArray();
    $( "#results" ).empty();
    jQuery.each( fields, function( i, field ) {
      $( "#results" ).append( field.value + " " );
    });
  }
  //

    $( "#submit_form" ).on( "click", function() {
        var formdata = $("#form_login").serializeArray();
        console.log(formdata);
        var data = {};
        $(formdata).each(function(index, obj){
            data[obj.name] = obj.value;
        });
        console.log(data);
        var request = $.ajax({
  url: "ajax.php",
  method: "POST", // gui du lieu di 
  data: data,
  dataType: "json"
});
 
request.done(function( data ) {
$('#message').html(data.message); $( "#message" ).animate({ "margin-left": "+=400px" }, "slow" );
$('#message').html(data.message);
});
 
request.fail(function( jqXHR, textStatus ) {
  alert( "Request failed: " + textStatus );
});
//   console.log( $( this ).text() );
});

</script>
</body>
</html>