<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-cli' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '^&`nQ1p5X{Up}{zARQ[*ToA_y:%$1x>a:xzj+KhF}zXFoZjWP*{%$0WETd{WG1sb' );
define( 'SECURE_AUTH_KEY',   'p1<+3ATFh,_H@tD<X:ksN35ab<O8HaHTJ-`<MsGfn}5whfWr<[&K/^$M/.WLw*br' );
define( 'LOGGED_IN_KEY',     'Y=fTi5*):s!1J])n!@HZBU8}H2[: R5z7{[<~u_o9F=5Zv?K$HL3$9T|7ZGUGu5e' );
define( 'NONCE_KEY',         '2<sYj!Aqmg]Xw6d=Q#(!Vkt[FGjo4u]R.wJ=8vKjQD#YjpSfA40~V()9K%lkc_$9' );
define( 'AUTH_SALT',         '2o({nm)2,,:MY!=HroMGf6?#EGz;B/7JFMfnbJ<C%yNyOyfCrdVUq$8=%_+G![LA' );
define( 'SECURE_AUTH_SALT',  '1q$v5f3]eG0k4l(_%5Y*A}8&LAj(;j&|J <G4)>{!T.eQI&><_&~cTHceMm;<:Yf' );
define( 'LOGGED_IN_SALT',    ';N&&%l;/Qwd#F&je #f{,etlrkqY]iATx0269Drbq[ WQp~;^VF4P9AsYDpJMzOe' );
define( 'NONCE_SALT',        '|rRK5_GCuWPJ2P^zpX0n?i=h{qg|zZel&[Ip]d>r|SR{C7R7}ZQCtzO^!~B((5Pr' );
define( 'WP_CACHE_KEY_SALT', '@XnHBw!!T/GnE~oFkn<E};kgrI)KxD>!MLCmY)TgGnD/`5Nvs0xq0oi|Pt,s_<F3' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
